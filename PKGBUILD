# This is an example PKGBUILD file. Use this as a start to creating your own,
# and remove these comments. For more information, see 'man PKGBUILD'.
# NOTE: Please fill out the license field for your package! If it is unknown,
# then please put 'unknown'.

# Maintainer: Future Linux Team <future_linux@163.com>
pkgname=(zlib minizip)
pkgbase=zlib
pkgver=1.3.1
pkgrel=3
pkgdesc="Compression library implementing the deflate compression method found in gzip and PKZIP"
arch=('x86_64')
url="https://www.zlib.net"
license=('Zlib')
source=(https://zlib.net/fossils/${pkgbase}-${pkgver}.tar.gz)
sha256sums=(9a93b2b7dfdac77ceba5a558a580e74667dd6fede4585b91eefb60f03b72df23)

prepare() {
	cd ${pkgbase}-${pkgver}/contrib/minizip

	cp Makefile Makefile.orig

	autoreconf -fiv
}

build() {
	cd ${pkgbase}-${pkgver}

	CFLAGS+=" -ffat-lto-objects"

	./configure --prefix=/usr --libdir=/usr/lib64

	make

	cd contrib/minizip

	./configure --prefix=/usr --libdir=/usr/lib64 --enable-static=no

	make

}

package_zlib() {
	depends=('glibc')

	cd ${pkgbase}-${pkgver}

	make DESTDIR=${pkgdir} install
}

package_minizip() {
	pkgdesc="Mini zip and unzip based on zlib"
	depends=('glibc' 'zlib')

	cd ${pkgbase}-${pkgver}/contrib/minizip

	make DESTDIR=${pkgdir} install


	# https://github.com/madler/zlib/pull/229
	rm ${pkgdir}/usr/include/minizip/crypt.h
}
